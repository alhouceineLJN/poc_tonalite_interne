# PoC Tonalite Interne

## Tonalite
### The data : 
The data generated and used for training the models can be found under :  `/home/alhouceine@ljnad.lajavaness.com/pe_mails/tonalite/data`

### The models : 
The models have been trained and are stored on the LJN GPU (Under `/home/alhouceine@ljnad.lajavaness.com/pe_mails/tonalite`). 

### The scripts : 
Preprocessing scripts (cleaning only + cleaning and modifying Annotto spans) as well as postprocessing scripts used for the Streamlit serving API.

As what concerns the training scripts, since we modified / did some bug fixes the saola NER code, you may find them in the feature branch created for that : ...

### The notebooks :

In this repo, you may find the experiments (in notebooks) performed and their results/scores as well as some other notebooks used for the error analysis.

## Causes :

### The data : 
The data generated and used for training the models can be found under :  `/home/alhouceine@ljnad.lajavaness.com/pe_mails/causes/data`

### The models : 
The models have been trained and are stored on the LJN GPU (Under `/home/alhouceine@ljnad.lajavaness.com/pe_mails/causes`). 
These models have been trained using saola-transfomers with a multilabel objective function/loss.

### The scripts : 
The same scripts used for the tonalite have been used for cleaning data for the multilabel model (we only took out 2-3 regexes -such as splitting the punctuations- )

### The notebooks :

In this repo, you may find the experiments (in notebooks) performed and their results/scores as well as some other notebooks used for the error analysis.
