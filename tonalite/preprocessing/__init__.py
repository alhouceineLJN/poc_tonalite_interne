import re
from typing import List, Sequence

from pydantic import BaseModel
from saola.std_input_output import TextEntitiesOutput

from preprocessing.clean_up import TO_REPLACE, URL_PATTERN

labels_map = {
    "E1": "satisfaction",
    "E2": "insatisfaction",
    "E3": "colère",
    "E4": "agressivité",
    "E5": "Tristesse",
    "E6": "Peur",
    "E7": "Sit grave",
    "E8": "Réclamation",
    "E10": "Urgence",
    "E11": "Joie",
}


def preprocessing(text: str, replace_guide: List[tuple] = TO_REPLACE):
    regex_applied = []
    text_modified = text
    for old, new in replace_guide:
        assert (
            re.match(old, new) is None
        ), f"Please write {old} in another format so that '{new}' does not match '{old}'."
        while (found := re.search(old, text_modified)) is not None:
            beg_pos, end_pos = found.span()
            piece_modified = text_modified[beg_pos:end_pos]
            text_modified = f"{text_modified[:beg_pos]}{new}{text_modified[end_pos:]}"
            regex_applied.append((old, new, beg_pos, end_pos, piece_modified))

    return text_modified, regex_applied


def ADD_CAPS_token_to_text(text: str):

    old_tokens = text.split()
    new_tokens = []
    prev_token_is_upper = False
    for token in old_tokens:
        if token.isupper() and (not prev_token_is_upper) and token != "USERNAME" and token != "RT":
            new_tokens.append("<s>NOTUSED")
            prev_token_is_upper = True
        elif (token.isupper() == False) and prev_token_is_upper:
            new_tokens.append("</s>NOTUSED")
            prev_token_is_upper = False

        new_tokens.append(token)

    return " ".join(new_tokens)


def postprocessing(text, regex_applied):
    text_reconstructed = text
    for _, char_to_replace, beg_pos, end_pos, char_to_include in reversed(regex_applied):
        decalage = len(char_to_replace) - len(char_to_include)
        text_reconstructed = f"{text_reconstructed[:beg_pos]}{char_to_include}{text_reconstructed[end_pos + decalage:]}"
    return text_reconstructed


def postprocessing_1(ents: TextEntitiesOutput):
    new_ents = []
    decalage = 0
    for ent in ents.ents:
        if ent.text == "<s>NOTUSED" or ent.text == "</s>NOTUSED":
            decalage += ent.end - ent.start + 1
        else:
            new_ents.append(
                Entity(
                    start=ent.start - decalage, end=ent.end - decalage, tag=ent.tag, text=ent.text
                )
            )
    return TextEntitiesOutput(ents=new_ents)


def postprocessing_2(text, regex_applied, entities):
    text_reconstructed = text
    new_entities = entities.ents.copy()

    for _, char_to_replace, beg_pos, end_pos, char_to_include in reversed(regex_applied):
        decalage = len(char_to_replace) - len(char_to_include)
        text_reconstructed = f"{text_reconstructed[:beg_pos]}{char_to_include}{text_reconstructed[end_pos + decalage:]}"

        modif = []
        for i in range(len(new_entities) - 1):
            if (new_entities[i].start == beg_pos) and (
                new_entities[i + 1].start == end_pos + decalage
            ):
                if new_entities[i].tag == new_entities[i + 1].tag:
                    modif.append(
                        {
                            "text": f"{new_entities[i].text}{new_entities[i+1].text}",
                            "tag": new_entities[i].tag,
                            "start": new_entities[i].start,
                            "end": new_entities[i + 1].start,
                        }
                    )
                else:
                    modif.append(
                        {
                            "text": f"{new_entities[i].text}",
                            "tag": new_entities[i].tag,
                            "start": new_entities[i].start,
                            "end": new_entities[i + 1].start,
                        }
                    )
                    modif.append(
                        {
                            "text": f"{new_entities[i+1].text}",
                            "tag": new_entities[i + 1].tag,
                            "start": new_entities[i + 1].start - 1,
                            "end": new_entities[i + 1].end - 1,
                        }
                    )
                break

            else:
                modif.append(
                    {
                        "text": f"{new_entities[i].text}",
                        "tag": new_entities[i].tag,
                        "start": new_entities[i].start,
                        "end": new_entities[i + 1].start,
                    }
                )

        new_entities = [
            Entity(start=ent["start"], end=ent["end"], tag=ent["tag"], text=ent["text"])
            for ent in modif
        ]

    # return text_reconstructed, new_entities

    return TextEntitiesOutput(ents=new_entities)


def postprocessing_3(entities):
    grouped_entities = []
    prev_tag = None
    current_entities = []
    for index, ent in enumerate(entities.ents):
        current_tag = ent.tag
        if (current_tag == prev_tag) or (prev_tag is None):
            current_entities.append(index)
        else:
            grouped_entities.append(current_entities)
            current_entities = [index]
        prev_tag = current_tag

    # if len(grouped_entities) == 0: # the case where there is only one tag in the sentence and the code does not go in the "else"
    grouped_entities.append(current_entities)

    new_entities = []
    for list_index in grouped_entities:
        if len(list_index) > 0:
            new_entities.append(
                Entity(
                    text=" ".join([entities.ents[i].text for i in list_index]),
                    start=entities.ents[list_index[0]].start,
                    end=entities.ents[list_index[-1]].end,
                    tag=labels_map[entities.ents[list_index[0]].tag],
                )
            )
    return TextEntitiesOutput(ents=new_entities)


class Entity(BaseModel):
    start: int
    end: int
    tag: str
    text: str
