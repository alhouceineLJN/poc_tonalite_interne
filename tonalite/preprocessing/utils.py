import json
from pathlib import Path
from typing import Dict, List

from sklearn.model_selection import train_test_split

FILE_FOR_SAOLA = "cleaned_annotations"
IOB_FILE_FOR_SAOLA = "iob_cleaned_annotations"


def read_jsonlines(path: Path):
    data = []
    with open(path, "r") as f:
        for line in f:
            data.append(json.loads(line))
    return data


def write_jsonlines(output_path: Path, dicos: List[Dict]):
    with open(output_path, "w") as f:
        for entry in dicos:
            json.dump(entry, f)
            f.write("\n")


def format_for_saola(item):
    labels = []
    if "ner" in (dico := item["annotation"]):
        for span in dico["ner"]["Tonalité-Émotion"]["entities"]:
            labels.append(
                {"start": span["start_char"], "end": span["end_char"], "tag": span["value"],}
            )
    return {
        "labels": labels,
        "text": item["item"]["data"]["text"],
        "uuid_orig": item["item"]["uuid"],
    }


def train_val_test(dir_path: Path, dicos: List[Dict], random_state=12):
    data_for_saola = []
    for index, item in enumerate(dicos):
        item_formatted = format_for_saola(item)
        if len(item_formatted) > 0:
            data_for_saola.append({**item_formatted, "uuid": index})

    train_data, val_test_data = train_test_split(
        data_for_saola, test_size=0.25, random_state=random_state
    )
    val_data, test_data = train_test_split(val_test_data, test_size=0.5, random_state=random_state)

    set_data = {"train": train_data, "validation": val_data, "test": test_data}

    for key, value in set_data.items():
        name = f"{FILE_FOR_SAOLA}.{key}.jsonlines"
        write_jsonlines(dir_path / name, value)


def offset_to_iob(spans_offsets, text):

    token_label = []
    text_split = text.split()
    sorted_spans = sorted(spans_offsets, key=lambda x: x["start"])
    pointer = 0
    word_counter = 0
    span_counter = 0

    while pointer < len(text) and word_counter < len(text_split):
        if text[pointer] == " ":
            pointer += 1
        if (len(sorted_spans) == 0) or (span_counter >= len(sorted_spans)):
            token_label.append(("O", text_split[word_counter]))
            pointer = pointer + len(text_split[word_counter]) + 1
            word_counter += 1
        else:
            span_treated = sorted_spans[span_counter]
            if pointer < span_treated["start"]:
                token_label.append(("O", text_split[word_counter]))
                pointer = pointer + len(text_split[word_counter]) + 1
                word_counter += 1
            else:
                tokens_in_span = text[span_treated["start"] : span_treated["end"]].split()
                token_label.append((f"B-{span_treated['tag']}", tokens_in_span[0]))
                word_counter += 1
                for i in range(1, len(tokens_in_span)):
                    token_label.append((f"I-{span_treated['tag']}", tokens_in_span[i]))
                    word_counter += 1

                pointer = span_treated["end"] + 1
                span_counter += 1

    return token_label


def iob_train_val_test(dir_path: Path, dicos: List[Dict], random_state=12):
    data_for_saola = []
    for index, item in enumerate(dicos):
        item_formatted = format_for_saola(item)
        if len(item_formatted) > 0:
            data_for_saola.append({**item_formatted, "uuid": index})

    train_data, val_test_data = train_test_split(
        data_for_saola, test_size=0.25, random_state=random_state
    )
    val_data, test_data = train_test_split(val_test_data, test_size=0.5, random_state=random_state)

    train_data_split = []
    for item in train_data:
        token_label = offset_to_iob(item["labels"], item["text"])
        train_data_split.append(
            {
                **item,
                "labels": [label for label, _ in token_label],
                "text": [token for _, token in token_label],
            }
        )

    val_data_split = []
    for item in val_data:
        token_label = offset_to_iob(item["labels"], item["text"])
        val_data_split.append(
            {
                **item,
                "labels": [label for label, _ in token_label],
                "text": [token for _, token in token_label],
            }
        )

    test_data_split = []
    for item in test_data:
        token_label = offset_to_iob(item["labels"], item["text"])
        test_data_split.append(
            {
                **item,
                "labels": [label for label, _ in token_label],
                "text": [token for _, token in token_label],
            }
        )

    set_data = {"train": train_data_split, "validation": val_data_split, "test": test_data_split}

    for key, value in set_data.items():
        name = f"{IOB_FILE_FOR_SAOLA}.{key}.jsonlines"
        write_jsonlines(dir_path / name, value)
