import json
import logging
import re
import sys
from pathlib import Path
from typing import List, Literal

import pandas as pd

URL_PATTERN = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"

TO_REPLACE = [
    # (URL_PATTERN, ""),
    (r"\.(?=\w)", ". "),  # .je suis --> . je suis
    (r"\,(?=\w)", ", "),
    (r"\?(?=\w)", "? "),
    (r"\;(?=\w)", "; "),
    (r"\!(?=\w)", "! "),
    (r"(?<=[A-Za-z0-9])\!", " !"),  # Quoi!!! --> Quoi !!!
    (r"(?<=[A-Za-z0-9])\?", " ?"),
    (r"\?(?=[\!\?])", "? "),  # splitting on punctuation experiment
    (r"\!(?=[\!\?])", "! "),  # splitting on punctuation experiment
    (r"\(Traduit par Google\) ?", ""),
    (r"\(Avis d'origine\).*$", ""),
    (r"^\s+", ""),
    (r"\s+$", ""),
    (r"\s{2,}", " "),
    # (r"\.{2,}", "..."),
    (r"(Visité en).+?$", ""),
    (r"(?<!\w)@\w+", "USERNAME"),
]


def translate(position: int, pivot: int, length_reduction: int, mode: Literal["start", "end"]):
    """Adapt the tag position to the right of pivot. Any tag to the right of pivot is translated by length_reduction characters to the left

    Args:
        position (int): Position (begin or end) of the tag
        pivot (int)
        length_reduction (int)
        mode(str): If "start", deal with the span beginning, if "end", deal with the span end

    Returns:
        int: New position of tag (begin or end)
    """
    if mode == "start":
        new_pos = max(pivot, position - length_reduction) if position > pivot else position
    else:
        new_pos = (
            max(position, pivot) - length_reduction
            if position > pivot - length_reduction
            else position
        )
    return new_pos


def replace_and_update(text: str, annotations: dict, replace_guide: List[tuple] = TO_REPLACE):
    """Replace the regex defined in replace_guide and adapt the tag positions

    Args:
        text (str): Original text
        annotations (dict): Original annotation, same format as Annotto

    Returns:
        (str, dict): New text, new annotation
    """
    for old, new in replace_guide:
        assert (
            re.match(old, new) is None
        ), f"Please write {old} in another format so that '{new}' does not match '{old}'."

        while (found := re.search(old, text)) is not None:
            beg_pos, end_pos = found.span()

            text = f"{text[:beg_pos]}{new}{text[end_pos:]}"
            length_reduction = end_pos - beg_pos - len(new)

            if "ner" in annotations and "Tonalité-Émotion" in annotations["ner"]:
                annotations["ner"]["Tonalité-Émotion"]["entities"] = [
                    {
                        **item,
                        "start_char": translate(
                            item["start_char"], beg_pos, length_reduction, "start"
                        ),
                        "end_char": translate(item["end_char"], end_pos, length_reduction, "end"),
                    }
                    for item in annotations["ner"]["Tonalité-Émotion"]["entities"]
                ]
    # fix spans beginnings and ends (when length of text is smaller than end_char, strips spaces from the start and end)
    if "ner" in annotations and "Tonalité-Émotion" in annotations["ner"]:
        for item in annotations["ner"]["Tonalité-Émotion"]["entities"]:

            if text[item["start_char"] : item["end_char"]][0] == " ":
                item["start_char"] = item["start_char"] + 1
            if text[item["start_char"] : item["end_char"]][-1] == " ":
                item["end_char"] = item["end_char"] - 1

        annotations["ner"]["Tonalité-Émotion"]["entities"] = [
            {**item, "end_char": min(item["end_char"], len(text))}
            for item in annotations["ner"]["Tonalité-Émotion"]["entities"]
        ]
    return text, annotations


if __name__ == "__main__":
    if len(sys.argv) != 3:
        logging.error(f"Please use {sys.argv[0]} input_file output_file")

    input_file = sys.argv[1]
    output_file = sys.argv[2]

    df = pd.read_json(input_file, lines=True)
    output_data = []

    for _, row in df.iterrows():
        if "To delete" in row["tags"]:
            continue
        rewritten_line = {
            "item": row["item"],
            "tags": row["tags"],
            "annotation": row["annotation"],
        }
        (
            rewritten_line["item"]["data"]["text"],
            rewritten_line["annotation"],
        ) = replace_and_update(row["item"]["data"]["text"], row["annotation"], TO_REPLACE)
        output_data.append(json.dumps(rewritten_line))

    Path(output_file).write_text("\n".join(output_data))
    logging.info(f"Clean data written into {output_file}")
