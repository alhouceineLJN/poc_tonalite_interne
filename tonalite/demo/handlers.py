from preprocessing import postprocessing_2, postprocessing_3, preprocessing
from saola.integration.fastapi.handlers import DefaultHandler
from saola.std_input_output import TextInput


class CustomHandler(DefaultHandler):
    def _preprocess(self, input):
        self.clean_input, self.regex_applied = preprocessing(input.text)
        return TextInput(text=self.clean_input)

    def _postprocess(self, output, input):
        processed_pred = postprocessing_2(self.clean_input, self.regex_applied, output)
        processed_pred = postprocessing_3(processed_pred)
        return processed_pred
