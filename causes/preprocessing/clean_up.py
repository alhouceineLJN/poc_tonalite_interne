import re
from typing import List

URL_PATTERN = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"

TO_REPLACE = [
    # (URL_PATTERN, ""),
    (r"\.(?=\w)", ". "),  # .je suis --> . je suis
    (r"\,(?=\w)", ", "),
    (r"\?(?=\w)", "? "),
    (r"\;(?=\w)", "; "),
    (r"\!(?=\w)", "! "),
    (r"(?<=[A-Za-z0-9])\!", " !"),  # Quoi!!! --> Quoi !!!
    (r"(?<=[A-Za-z0-9])\?", " ?"),
    (r"\(Traduit par Google\) ?", ""),
    (r"\(Avis d'origine\).*$", ""),
    (r"^\s+", ""),
    (r"\s+$", ""),
    (r"\s{2,}", " "),
    # (r"\.{2,}", "..."),
    (r"(?<!\w)@\w+", "USERNAME"),
]


def preprocessing(text: str, replace_guide: List[tuple] = TO_REPLACE):
    text_modified = text
    for old, new in replace_guide:
        assert (
            re.match(old, new) is None
        ), f"Please write {old} in another format so that '{new}' does not match '{old}'."
        while (found := re.search(old, text_modified)) is not None:
            beg_pos, end_pos = found.span()
            piece_modified = text_modified[beg_pos:end_pos]
            text_modified = f"{text_modified[:beg_pos]}{new}{text_modified[end_pos:]}"

    return text_modified
