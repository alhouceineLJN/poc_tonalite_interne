import json
import uuid
from pathlib import Path
from typing import Dict, List

from sklearn.model_selection import train_test_split

from preprocessing.clean_up import preprocessing

FILE_FOR_SAOLA = "annotations"
CLEAN_FILE_FOR_SAOLA = "cleaned_annotations"
FILE_CAUSES_FOR_ANNOTTO = "ANNOTTO_spans_for_cause"

MAPPING_LBL_TONALITE = {
    "E1": "Satisfait",
    "E2": "Insatisfait",
    "E3": "Colère",
    "E4": "Aggressivité Conflit",
    "E5": "Tristesse Deception Démotivation",
    "E6": "Peur",
    "E7": "Détresse - Situation grave",
    "E8": "Réclamation",
    "E10": "Urgence",
    "E11": "Joie Surprise positive",
}


def read_jsonlines(path: Path):
    data = []
    with open(path, "r") as f:
        for line in f:
            data.append(json.loads(line))
    return data


def write_jsonlines(output_path: Path, dicos: List[Dict]):
    with open(output_path, "w") as f:
        for entry in dicos:
            json.dump(entry, f)
            f.write("\n")


def create_annotto_causes_project(
    output_dir: Path, dicos: List[Dict],
):
    """
    Generate annotto compatible items from the output of annotto Tonalité NER project
    """

    cause_dataset = []

    for item in dicos:
        full_text = item["item"]["data"]["text"]
        uuid = item["item"]["uuid"]
        annotations = item["annotation"]
        if "ner" not in annotations:
            continue

        spans = annotations["ner"]["Tonalité-Émotion"]["entities"]
        for span in spans:
            cause_dataset.append(
                (
                    uuid,
                    " ".join(
                        [
                            "[" + MAPPING_LBL_TONALITE[span["value"]] + "]",
                            full_text[span["start_char"] : span["end_char"]],
                        ]
                    ),
                    full_text,
                )
            )

    data_for_annotto = []
    for dic in cause_dataset:
        dic_annotto = dict({"datatype": "text"})
        dic_annotto["uuid"] = uuid.uuid4().hex
        dic_annotto["data"] = {"text": dic[1]}
        dic_annotto["metadata"] = {"full_text_uuid": dic[0], "full_text": dic[2]}
        data_for_annotto.append(dic_annotto)

    name = f"{FILE_CAUSES_FOR_ANNOTTO}.jl"
    write_jsonlines(output_dir / name, data_for_annotto)


def format_for_saola(item: Dict):
    """
    Format annotto items of Causes project into compatible items for saola model
    """
    labels = []
    if "classifications" in (dico := item["annotation"]):
        if "Causes" in (dico_ := dico["classifications"]):
            for cls in dico_["Causes"]["labels"]:
                labels.append(cls["value"])
    if len(labels) == 0:
        return {}

    list_ton_text = item["item"]["data"]["text"].split("] ")
    context = item["item"]["metadata"]["full_text"].replace(list_ton_text[1], "")
    tonalite = list_ton_text[0].replace("[", "")
    result = {
        "uuid_orig": item["item"]["uuid"],
        "uuid_full_text_orig": item["item"]["metadata"]["full_text_uuid"],
        "text": f"{list_ton_text[1]}",
        "full_text": item["item"]["metadata"]["full_text"],
        "main_tonalite": f"{tonalite}",
        "text_w_context": f"{list_ton_text[1]} </s> {context}",
        "text_w_tonalite": f'{item["item"]["data"]["text"]}',
    }
    for i in range(1, 14):
        result[f"C{i}"] = 0
    for lbl in labels:
        result[lbl] = 1
    return result


def train_val_test(dir_path: Path, dicos: List[Dict], random_state=12):
    data_for_saola = []
    for index, item in enumerate(dicos):
        item_formatted = format_for_saola(item)
        if len(item_formatted) > 0:
            data_for_saola.append({**item_formatted, "uuid": index})

    train_data, val_test_data = train_test_split(
        data_for_saola, test_size=0.25, random_state=random_state
    )
    val_data, test_data = train_test_split(val_test_data, test_size=0.5, random_state=random_state)

    set_data = {"train": train_data, "validation": val_data, "test": test_data}

    for key, value in set_data.items():
        name = f"{FILE_FOR_SAOLA}.{key}.jsonlines"
        write_jsonlines(dir_path / name, value)


def format_clean_for_saola(item: Dict, option="default"):
    """
    Format and clean (process) annotto items of Causes project into compatible items for saola model
    option : 
        - default : gives a one hot encoding of the labels
        - excl_C9 : the class 9 (aucune cause) is excluded from the output
        - excl_C9_implications : the class 9 (aucune cause) is excluded from the output and having C12=1 or C13=1 forces C4=1
    """
    labels = []
    if "classifications" in (dico := item["annotation"]):
        if "Causes" in (dico_ := dico["classifications"]):
            for cls in dico_["Causes"]["labels"]:
                labels.append(cls["value"])
    if len(labels) == 0:
        return {}

    list_ton_text = item["item"]["data"]["text"].split("] ")
    clean_text = preprocessing(list_ton_text[1])
    clean_full_text = preprocessing(item["item"]["metadata"]["full_text"])
    context = clean_full_text.replace(clean_text, "")

    result = {
        "uuid_orig": item["item"]["uuid"],
        "uuid_full_text_orig": item["item"]["metadata"]["full_text_uuid"],
        "text": f"{clean_text}",
        "full_text": clean_full_text,
        "main_tonalite": f"{list_ton_text[0]}".replace("[", ""),
        "text_w_context": f"{clean_text} </s> {context}",
        "text_w_tonalite": f'{item["item"]["data"]["text"]}',
    }
    if option == "default":
        for i in range(1, 14):
            result[f"C{i}"] = 0
        for lbl in labels:
            result[lbl] = 1
    elif option == "excl_C9":
        for i in range(1, 14):
            if i != 9:
                result[f"C{i}"] = 0
        for lbl in labels:
            if lbl != "C9":
                result[lbl] = 1
    elif option == "excl_C9_implications":
        for i in range(1, 14):
            if i != 9:
                result[f"C{i}"] = 0
        for lbl in labels:
            if lbl != "C9":
                result[lbl] = 1
            if lbl == "C12":
                result["C4"] = 1
            if lbl == "C13":
                result["C4"] = 1
    return result


def clean_train_val_test(dir_path: Path, dicos: List[Dict], random_state=12, option="default"):
    data_for_saola = []
    for index, item in enumerate(dicos):
        item_formatted = format_clean_for_saola(item, option=option)
        if len(item_formatted) > 0:
            data_for_saola.append({**item_formatted, "uuid": index})

    train_data, val_test_data = train_test_split(
        data_for_saola, test_size=0.25, random_state=random_state
    )
    val_data, test_data = train_test_split(val_test_data, test_size=0.5, random_state=random_state)

    set_data = {"train": train_data, "validation": val_data, "test": test_data}
    if option == "default":
        option_str = ""
    elif option == "excl_C9":
        option_str = "_C9-excluded"
    elif option == "excl_C9_implications":
        option_str = "_C9-excluded_C12-C13-implies-C4"

    for key, value in set_data.items():
        name = f"{CLEAN_FILE_FOR_SAOLA}{option_str}.{key}.jsonlines"
        write_jsonlines(dir_path / name, value)

